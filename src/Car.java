public class Car {
/*
    4 things in a class
    1. Properties/attributes - the characteristics of the object the class will create.

    2. Constructor - method to create the object and instantiate with its initialized values.

    3. Getters and setters - are methods to get the values of an object's properties or set them.

    4. Methods - actions that an object can perform or do.

    Access Modifier (Public and Private)
    public access - the variable/property in the class is accessible anywhere in the application.

    Attributes/properties of a class should not be made public. They should only be accessed with getters and setters instead of just dot notation.
    private - limits the access and ability to get or set a variable/method to only within its own class.
    getters - methods that returns the value of the property.
    setters - methods that allow us to set the value of a property.
*/

    private String make;

    private String brand;

    private int price;

    private Driver carDriver;

    // Constructor is a method which allows us to set the initial values of an instance
    // empty/default constructor - allows us to create an instance with default initialized values.
    // By default, Java, when your class does not have a constructor, assigns one for you. Java also sets the default values. You could have a way to add your own default values.

    public Car(){
        // this.brand = "Geely";
        this.carDriver = new Driver();
    }

    public Car(String make,String brand,int price, Driver driver){

        this.make = make;
        this.brand = brand;
        this.price = price;
        this.carDriver = driver;

    }
    //Getters and Setter for our properties
    //Getters return a value so therefore we must add the dataType of the value returned.
    public String getMake(){
        //this keyword refers to the object/instance where the constructor or setter/getter is.
        return this.make;
    }

    public String getBrand(){
        return this.brand;
    }

    public int getPrice(){
        //this keyword refers to the object/instance where the constructor or setter/getter is.
        return this.price;
    }

    public void setMake(String makeParams){
        this.make = makeParams;
    }

    public void setBrand(String brandParams){
        this.brand = brandParams;
    }

    public void setPrice(int priceParams){
        this.price = priceParams;
    }

    /*
        Mini Activity
        Create getters and setters for the brand and price properties.
        In Main, update the brand of car1 and display it in the terminal.
        In Main, update the price of car1 and display it in the terminal.
    */

    //You could set a property as read-only, this means that the value of the property can only be get but not updated.

    //Methods are functions of an object.instance which allows us to perform certain tasks.
    //void - means that the function does not return anything. Because in Java, a method's return dataType must also be declared.
    public void start(){
        System.out.println("Vroooom! Vrooom!");
    }

    //Classes have relationship
    /*
        Composition allows modelling objects to be made up of other objects. Classes can have instances of other classes.

        A Car has a Driver.
    */

    public Driver getCarDriver() {
        return carDriver;
    }

    public void setCarDriver(Driver carDriver) {
        this.carDriver = carDriver;
    }

    //custom method to retrieve the car driver's name:
    public String getCarDriverName(){
        return this.carDriver.getName();
    }
}
