public class Main {
    public static void main(String[] args) {
        // System.out.println("Hello world!");

        Car car1 = new Car();
        // System.out.println(car1.brand);
        // System.out.println(car1.make);
        // System.out.println(car1.price);
//        car1.make = "Vayron";
//        car1.brand = "Bugatti";
//        car1.price = 2000000;
//        System.out.println(car1.brand);
//        System.out.println(car1.make);
//        System.out.println(car1.price);
        //System.out.println(car1); result: memory address in the machine
        // Instance - an object created from a class and each instance of a class should be independent from one another.
        Car car2 = new Car();
        // System.out.println(car2.brand);
//        car2.make = "Tamaraw FX";
//        car2.brand = "Toyota";
//        car2.price = 450000;
//        System.out.println(car2.brand);
//        System.out.println(car2.make);
//        System.out.println(car2.price);
        // We cannot add value to a property that does not exist or defined in our class.
        // car2.driver = "Alejandro";

        /*
            Mini-Activity:

            Create a new instance of the Car class and save it in a variable called car3.
            Access the properties of the instance and update its values.
                make = String
                brand = String
                price = int
            You can come up with your own values.
            Print the values of each property of the instance.
        */

        Car car3 = new Car();
//        car3.make = "Hilux";
//        car3.brand = "Toyota";
//        car3.price = 2500000;
//        System.out.println(car3.brand);
//        System.out.println(car3.make);
//        System.out.println(car3.price);
//
        Car car4 = new Car();
//        System.out.println(car4.brand);
//
        Driver driver1 = new Driver("Alejandro",25);

        Car car5 = new Car("Vios","Toyota",1500000,driver1);
//        System.out.println(car5.make);
//        System.out.println(car5.brand);
//        System.out.println(car5.price);
//
        car1.start();
        car2.start();
        car3.start();
        car4.start();
        car5.start();

        //make property getters
        System.out.println(car1.getMake());
        System.out.println(car5.getMake());

        //make property setter
        car1.setMake("Veyron");
        System.out.println(car1.getMake());
        car1.setBrand("Bugatti");
        System.out.println(car1.getBrand());
        car1.setPrice(2500000);
        System.out.println(car1.getPrice());

        car5.setMake("Innova");
        System.out.println(car5.getMake());

        System.out.println(car5.getCarDriver().getName());

        Driver newDriver = new Driver("Antonio",21);
        car5.setCarDriver(newDriver);

        //get name of new carDriver
        System.out.println(car5.getCarDriver().getName());
        System.out.println(car5.getCarDriver().getAge());
        //custom getCarDriverName method
        System.out.println(car5.getCarDriverName());

        /*
            Activity

            Create a new class called Animal with the following attributes:
                name - string
                color - string

            Add the constructors, getters and setters for the class.

            Add a method called, call(). This method is public and will simply print a message to introduce the animal:
            "Hi! My name is <nameOfAnimal>"

            Instantiate a new Animal object in the main method.

            Invoke the animal's call() method.
        */

        Animal dog = new Animal("Beagle","White, Brown and Black");
        System.out.println(dog.call());
    }
}