public class Dog extends Animal{

    //extends keyword allows us to have this class inherit the attributes and methods of the Animal class
    //Parent class is the class where we inherit from.
    //Child class/sub-class a class that inehrits from a parent.
    //No, Sub-class cannot inherit from multiple parents.
    //Instead sub-class can "multiple inherit" from what we call interfaces.

}
